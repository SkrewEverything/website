import React from 'react';
import ReactDOM from 'react-dom';

/**
 * React Components
 */
import { TW } from '../components/typewriter/typewriter';
import { ICH } from '../components/individual-char-highlight/individual-char-highlight';
import { SMW } from '../components/SidemenuWrapper/SidemenuWrapper';

/**
 * CSS Files
 */
import s from './index.css';
import position from '../components/css/center-components.css';

/**
 * Images
 */
import logo from '../images/eyes-final copy copy.svg';
import projects from '../images/side/project-logo.svg';
import github from '../images/side/github-logo.svg';
import so from '../images/side/stackoverflow-logo.svg';
import mail from '../images/side/mail-logo.svg';

class App extends React.Component {
	render() {
		let TWStyle = {
			position: 'relative',
			left: '5%'
		};

		return (
			<div className={s.main}>
				<SMW
					images={[projects, github, so, mail]}
					names={['Projects', 'Github', 'Stack', 'Mail']}
					links={[
						'projects',
						'https://github.com/SkrewEverything',
						'https://stackoverflow.com/users/4859791/skreweverything?tab=profile',
						'mailto:skreweverything@gmail.com'
					]}
					target={['_self', '_blank', '_blank', '_self']}
				/>
				<div className={position['center-center']}>
					<div style={{ height: '10%' }} />
					<img src={logo} className={s.logo} />
					<TW
						data={['{SkrewEverything}', '{S}']}
						passTo="ICH"
						style={TWStyle}
						location="home"
					/>
					<div style={{ height: '10%' }} />
					<ICH data=".Never do anything yourself that a computer can do for you." />
					<div style={{ height: '30%' }} />
				</div>
			</div>
		);
	}
}

ReactDOM.render(
	<div className={s.innerAppDiv}>
		<App />
	</div>,
	document.getElementById('app')
);
