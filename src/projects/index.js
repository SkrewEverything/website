import React from 'react';
import ReactDOM from 'react-dom';

/**
 * React Components
 */
import { TW } from '../components/typewriter/typewriter';
import { Intro } from '../components/introduction/introduction';
import { PS } from '../components/project-summary/project-summary';
import { SMW } from '../components/SidemenuWrapper/SidemenuWrapper';

/**
 * CSS Files
 */
import s from './index.css';
import c from '../components/css/center-components.css';

/**
 * Images
 */
import home from '../images/side/home-logo.svg';
import github from '../images/side/github-logo.svg';
import so from '../images/side/stackoverflow-logo.svg';
import mail from '../images/side/mail-logo.svg';
import keylogger from '../images/projects/keylogger-logo.svg';
import adrive from '../images/projects/a-drive.svg';
import sqlight from '../images/projects/sqlight.svg';
import webtracker from '../images/projects/web-tracker.svg';

class App extends React.Component {
	render() {
		let TWStyle = {
			fontSize: '7vw',
			color: 'green',
			fontFamily: 'Courier New, monospace'
		};

		let promptStyle = TWStyle;

		return (
			<div>
				<SMW
					images={[home, github, so, mail]}
					names={['Home', 'Github', 'Stack', 'Mail']}
					links={[
						'/',
						'https://github.com/SkrewEverything',
						'https://stackoverflow.com/users/4859791/skreweverything?tab=profile',
						'mailto:skreweverything@gmail.com'
					]}
					target={['_self', '_blank', '_blank', '_self']}
				/>
				<div className={s['type-animation']}>
					<span
						style={promptStyle}
						className={s['animation-position']}>
						$
						<TW
							data={['{SkrewEverything}', '{S}']}
							style={TWStyle}
							passTo="ICH"
							location="projects"
						/>
					</span>
				</div>
				<div className={s.matter}>
					<Intro />
					<hr />
					<PS
						image={webtracker}
						title={'Web Tracker - Log the Browser'}
						summary={[
							'It scans and logs the currently opened websites in Chrome Browser. It can even log incognito windows.',
							"And the main thing is It doesn't require any permissions to run🔥."
						]}
						link={{
							address:
								'https://github.com/SkrewEverything/Web-Tracker',
							target: '_blank'
						}}
					/>
					<PS
						image={keylogger}
						title={"Track'em - Keylogger for macs"}
						summary={[
							'It is a simple and easy to use keylogger for macOS. It is not meant to be malicious. There are only few keyloggers available for mac and none of them are in swift with low-level access.',
							'Most of the keyloggers available only logs keystrokes into a file without much information about on which app the keystrokes are generated.'
						]}
						link={{
							address:
								'https://github.com/SkrewEverything/Swift-Keylogger',
							target: '_blank'
						}}
					/>
					<PS
						image={sqlight}
						title={'SQLight - A simple Swift layer over SQLite3'}
						summary={
							'Working with SQLite3 in Swift is usually a headache due to the data types used in C. SQLight handles all the headaches for you so that you can peacefully use it with Swift.'
						}
						link={{
							address:
								'https://github.com/SkrewEverything/SQLight',
							target: '_blank'
						}}
					/>
					<PS
						image={adrive}
						title={'A-Drive - Authentication Drive'}
						summary={
							'I made this project for Google Science Fair 2015. It is a thought about implementing secure login using physical device on client side.'
						}
						link={{
							address:
								'https://docs.google.com/presentation/d/1InxucBI1_ZLBqmBz37NiZbbz4LBaNHPPFlTbcjpGyAM/edit#slide=id.p',
							target: '_blank'
						}}
					/>
				</div>
			</div>
		);
	}
}

ReactDOM.render(<App />, document.getElementById('app'));
