import React from 'react';
import s from './introduction.css';

/**
 * A small introduction in the `projects` page
 */

export class Intro extends React.Component {
	render() {
		return (
			<div>
				<h1>PROJECTS</h1>
				<br />
				<br />
				<div>
					Here are some projects I have been working on during my free
					time. Feel free to contact me at
					<span className={s.mail}> skreweverything@gmail.com </span>.
					I would be glad to hear about issues, comments or any other
					sort of feedback.
				</div>
				<br />
			</div>
		);
	}
}
