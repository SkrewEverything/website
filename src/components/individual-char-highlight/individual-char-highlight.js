import React from 'react';
import style from './individual-char-highlight.css';

/**
* Returns a component which will change color when hovered
* It takes a string and returns an array of `span`s
* Each span is individual character when hovered changes only that character color
* *************************
* *************************
* Description of props:
*
* "Needed props"
* name: `this.props.data`, type: String -> Returns an array of `span`s where each span is individual character when hovered changes only that character color
* *************************
*  "Optional props"
*  name: `this.props.name`, type: String, default: `data` -> Pass the custom `className` to add the custom `className` to the span. Can be used to apply specific CSS. Add CSS rules in individual-char-highlight.css
*
* "Note"
* If you want different styles for the `span`s, then add styles in the `individual-char-highlight.css` file with custom `className` and add it to span like mentioned in `this.props.name` above
*
*/

export class ICH extends React.Component {
	constructor(props) {
		super(props);

		// - Mark methods
		this.addClass = this.addClass.bind(this);
		this.createSpans = this.createSpans.bind(this);
	}

	/**
   * addClass - Adds the class name for CSS styling.
   *
   *
   * @param  {Array of spans} data It is returned from this.createSpans().
   * @return {JSX element}      JSX element with desired class and can be returned in the render().
   */
	addClass(data) {
		return <span className={style[this.props.name]}>{data}</span>;
	}

	/**
   * createSpans - Splits a string and inserts one char inside each span
   *
   * @return {Array of spans}  Array of spans which should be passed to this.addClass() to add the class name for CSS styling.
   */
	createSpans() {
		var data = [];
		for (var i = 0; i < this.props.data.length; i++) {
			data.push(<span key={i}>{this.props.data[i]}</span>);
		}
		return data;
	}

	render() {
		var data = this.createSpans();

		return this.addClass(data);
	}
}

ICH.defaultProps = { name: 'data' };
