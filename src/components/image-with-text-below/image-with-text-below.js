import React from 'react';
import s from './image-with-text-below.css';

/**
 * Creates a long vertical div with image and text below it.
 * Each image and its name is wrapped inside an anchor tag.
 **********************
 **********************
 * Description of props:
 *
 * "Needed props"
 * name: `this.props.images`, type: `Array of images` -> The images that need to be displayed
 * name: `this.props.names`, type: `Array of Strings` -> The names that need to be displayed under the image.
 * name: `this.props.links`, type: `Array of Strings` -> Each image and text is wrapped around this URL using anchor tag
 * name: `this.props.targer`, type: `Array of Strings`, -> Specifies how to open the clicked links.
 *
 */

export class IWT extends React.Component {
	constructor(props) {
		super(props);
		this.createDiv = this.createDiv.bind(this);
	}

	createDiv() {
		var imgArray = [];
		for (var i = 0; i < this.props.images.length; i++) {
			imgArray.push(
				<div key={i} className={s['image-divs']}>
					<a
						className={s['link']}
						href={this.props.links[i]}
						target={this.props.target[i]}>
						<img
							className={s['image']}
							src={this.props.images[i]}
						/>
						<p className={s['text']}>{this.props.names[i]}</p>
					</a>
				</div>
			);
		}

		return imgArray;
	}

	// `outer-div` is used to scroll the div vertically if there are more images to display
	// and also to prevent the overlapping on to the `closing-button`
	// This `outer-div` is placed inside the `Sidemenu` component.
	render() {
		return <div className={s['outer-div']}>{this.createDiv()}</div>;
	}
}
