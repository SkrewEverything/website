import React from 'react';
import s from './sidemenu.css';

/**
 * Handles the sidemenu animation. It is fixed into position using `position:fixed`
 * *********************
 * *********************
 * Description of props:
 *
 * "Needed props"
 * name: `this.props.sideMenuActive` type:'boolean' -> Whether to show or not
 *
 * name: `this.props.children` type:`Array of image divs from image-with-text-below component` -> It is included in between the sidemenu component
 */

export class Sidemenu extends React.Component {
	constructor(props) {
		super(props);

		/** Initially `this.props.sideMenuActive` is `false`, so when the page loads,
		 *  `hide-menu` class is applied which starts the hiding animation.
		 *  So to prevent it, we use this. Make it to `false` when it is encountered for first time.
		*/
		this.initial = true;
	}

	render() {
		let sidemenuDiv;

		if (this.props.sideMenuActive) {
			sidemenuDiv = (
				<div className={s.vertical + ' ' + s['show-menu']}>
					{this.props.children}
				</div>
			);
		} else {
			// This is the initial time where it prevents the `hide-menu` animation. It just hides the menu.
			if (this.initial) {
				sidemenuDiv = (
					<div className={s.vertical}>{this.props.children}</div>
				);
				// Change it to `false` so that this block is never executed and `hide-menu` animation class is applied.
				this.initial = false;
			} else {
				sidemenuDiv = (
					<div className={s.vertical + ' ' + s['hide-menu']}>
						{this.props.children}
					</div>
				);
			}
		}

		return sidemenuDiv;
	}
}
