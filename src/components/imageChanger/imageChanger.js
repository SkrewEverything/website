import React from 'react';
import s from './imageChanger.css';

/**
 * Images get changed when the image is clicked
 * *************************
 * *************************
 * Description of props:
 *
 * "Needed props"
 * name: `this.props.images[]`, type: `Array of images` -> Images needed to change between. Currently changes between only two images. `0` and `1`
 * *************************
 * "Optional props"
 * name: `this.props.handler()`, type: `function` -> It can be used to pass a function from parent which can update the state of the parent by calling `this.setState()`
 */
export class ImageChanger extends React.Component {
	constructor(props) {
		super(props);

		/**
     * `isActive`: We are only changing 2 images, so `isActive` is a boolean having 2 states. If we want multiple images, update the `isActive` to a number to keep track of which image it is showing currently.
     * `image`: It is the image which displays the current image from `this.props.images[]`
     * `cssName`: To change the CSS classname which executes an animation when clicked.
     *
     * TODO: Improve `isActive` to support more than 2 images. Improve `cssName` animation in `imageChanger.css`
     */
		this.state = {
			isActive: false,
			image: this.props.images[0],
			cssName: s.change
		};

		// - Mark methods
		this.changeImage = this.changeImage.bind(this);
	}

	/**
   * changeImage - When the image is clicked, this function is called to update the state(image, css styling class etc)
   *
   */
	changeImage() {
		if (this.state.isActive) {
			this.setState({
				isActive: false,
				image: this.props.images[0],
				cssName: s.change
			});

			/**
       		* It calls the parent method to update the state in the parent
       		*/
			this.props.handler();
		} else {
			this.setState({
				isActive: true,
				image: this.props.images[1],
				cssName: s.change1
			});
			/**
       * It calls the parent method to update the state in the parent
       */
			this.props.handler();
		}
	}

	render() {
		return (
			<img
				className={this.state.cssName}
				src={this.state.image}
				onClick={this.changeImage}
				style={{ height: '8vh' }}
			/>
		);
	}
}
