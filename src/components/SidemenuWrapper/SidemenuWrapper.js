import React from 'react';
import { ImageChanger } from '../imageChanger/imageChanger';
import { Sidemenu } from '../sidemenu/sidemenu';
import { IWT } from '../image-with-text-below/image-with-text-below';

import network from '../../images/network.svg';
import cancel from '../../images/delete-button.svg';

import s from './SidemenuWrapper.css';

/**
 * This acts a wrapper component for `imageChanger`, `sidemenu` and `image-with-text-below` components
 * Combining all components into one component, a proper SIDEMENU is created, displayed and hidden
 * *************************
 * *************************
 * Description of props:
 *
 * "Needed props"
 * name: `this.props.images`, type: `Array of paths` -> Images to be displayed in the sidemenu
 * name: `this.props.names`, type:`Array of Strings` -> Names to be displayed under the image in the sidemenu
 * name: `this.props.links`, type:`Array of Strings` -> URLs when the image and name is clicked
 * name: `this.props.target`, type:`Array of Strings` -> Specifies how to open the clicked links.
 *
 * All these props are send to `image-with-text-below`
 */

export class SMW extends React.Component {
	constructor(props) {
		super(props);

		/**
		 * `rotateMenuLogo`: whether to rotate menu-logo or not.
		 * `sideMenuActive`: whether side menu is showing or not. It is sent to `Sidemenu` component which shows or hides the side menu
		 */
		this.state = {
			rotateMenuLogo: true,
			sideMenuActive: false
		};

		this.changeMenuRotate = this.changeMenuRotate.bind(this);
	}

	/**
	 * changeMenuRotate - 	Changes the logo, rotation and sidemenu.
	 * 						It is sent as a props to `ImageChanger` component.
	 * 						When the menu logo is clicked, this method is called to change the state.
	 */

	changeMenuRotate() {
		this.setState({
			rotateMenuLogo: !this.state.rotateMenuLogo,
			sideMenuActive: !this.state.sideMenuActive
		});
	}

	render() {
		// ImageChanger component
		let icComponent = (
			<ImageChanger
				images={[network, cancel]}
				handler={this.changeMenuRotate}
			/>
		);
		// Holds the image. What to display
		let menuLogoDiv;

		// Whether to add the rotation to the menu logo or not
		if (this.state.rotateMenuLogo) {
			menuLogoDiv = (
				<div className={s['menu-logo-rotate']}>{icComponent}</div>
			);
		} else {
			menuLogoDiv = (
				<div className={s['menu-logo-dont-rotate']}>{icComponent}</div>
			);
		}

		return (
			<div>
				{menuLogoDiv}
				<Sidemenu sideMenuActive={this.state.sideMenuActive}>
					<IWT
						images={this.props.images}
						names={this.props.names}
						links={this.props.links}
						target={this.props.target}
					/>
				</Sidemenu>
			</div>
		);
	}
}
