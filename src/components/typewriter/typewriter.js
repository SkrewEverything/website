import React from 'react';
import { ICH } from '../individual-char-highlight/individual-char-highlight';
import b from '../css/blink.css'; // For the cursor blink animation
import s from './typewriter.css'; // Default styles

/**
 * By default, it returns a component which produces typewriter animation on the array of strings passed as `this.props.data`
 * *************************
 * *************************
 * Description of props:
 *
 * "Needed props"
 * name: `this.props.data[]`, type: Array(String)-> Produces typewriter animation on the array of strings passed
 * *************************
 * "Optional props"
 * name: `this.props.time`, type: Integer, default: 60 -> Milliseconds to delete or add one character to the screen
 *
 * name: `this.props.name`, type: String, default: `data` -> Pass the custom `className` to add the custom `className` to the span. Can be used to apply specific CSS. Add CSS rules in typewriter.css
 *
 * name: `this.props.passTo`, type: String, default: undefined -> Used to pass the data to specified component after final string is constructed before it is displayed.
 *
 * name: `this.props.style`, type: Style Object, default: null -> Used to override default CSS styles. The passed style object applies to both string and cursor.
 *
 * name: `this.props.location`, type: String, default: undefined -> Used to specify the webpage location
 */

export class TW extends React.Component {
	constructor(props) {
		super(props);

		// Initial state
		this.state = {
			data: ''
		};

		// - Mark methods
		this.animate = this.animate.bind(this);
		this.updateState = this.updateState.bind(this);
		this.modify = this.modify.bind(this);
		this.animate();
	}

	/**
     * animate - Typing Animation logic
     *
     */
	animate() {
		var dataIndex = 0; // It is used to iterate the `this.props.data`
		var d = this.props.data[dataIndex]; // Current string to apply animation
		var strLength = d.length; // Length of current string
		var text = this.props.data; // It is the string that changes to produce type writer effect
		var flag = true; // It is used to add or remove the character from `text`
		var index = strLength; // It is used in `substr` to specify the `to` argument and from is always `0`
		var count = 0; // It is used to delay the adding of first character when `text` is empty.

		setInterval(
			function() {
				if (text.length == 0) {
					// If the string `text` is empty
					if (count == 15) {
						// Initially `0` waits till it gets to `15` and then adds the char
						dataIndex =
							dataIndex >= this.props.data.length - 1
								? 0
								: ++dataIndex;
						d = this.props.data[dataIndex]; // Update the string to apply animation on
						strLength = d.length; // Update the length of current string
						index = 1; // Increment `index` to add the 1st char
						flag = true; // Make it true so it adds chars one-by-one in next `else if`
						count = 0; // Again make it `0` for next iteration
					} else {
						count++; // Increment `count` when `text` is empty untill `count` = `15`
					}
				} else if (strLength + 20 == index) {
					// same like `count`, `+20` delays the deletion after the `text` is full
					index--; // Decrement `index` to delete the last char.
					flag = false; // Make it `false` so it delets chars one-by-one in next `else if`
				} else if (flag) {
					// It executes to add the chars to `text` one-by-one
					index++;
				} else if (!flag) {
					// It executes to delete the chars from `text` one-by-one
					index--;
				}

				text = d.substr(0, index); // Display the string, even if it's empty
				this.updateState(text); // Call to update the `this.state.data`
			}.bind(this),
			this.props.time
		); // Change this number to change the typing speed. Less = Fast
	}

	/**
     * updateState - Updates the state with current `text` to call the render.
     *
     * @param  {String} text Displays the constructed `text` string
     *
     */
	updateState(text) {
		this.setState({
			// Display the constructed `text` string
			data: text
		});
	}

	/**
     * modify - Sends the constructed string to another component for further components if specified any. Otherwise returns unmodified string as component
     *          Also adds custom in-line CSS styles
     *          Also applys specific CSS from external file. Add CSS rules in typewriter.css
     *
     * @return {span}  Span which might be modified by other components or not
     */
	modify() {
		// To return the constructed final component to render
		let mainData;

		if (this.props.passTo == 'ICH') {
			mainData = [
				<span
					className={s[this.props.name]}
					style={this.props.style}
					key={0}>
					<ICH data={this.state.data} name={this.props.location} />
				</span>,
				<span
					className={b['blink'] + ' ' + s['cursor']}
					style={this.props.style}
					key={1}>
					&#9614;
				</span>
			];
		} else {
			mainData = [
				<span
					className={s[this.props.name]}
					style={this.props.style}
					key={0}>
					{this.state.data}
				</span>,
				<span
					className={b['blink'] + ' ' + s['cursor']}
					style={this.props.style}
					key={1}>
					&#9614;
				</span>
			];
		}
		return mainData;
	}

	render() {
		var displayData = this.modify(); // Final span to be displayed after all the modifications

		return <span>{displayData}</span>;
	}
}

/**
 * Default value of `this.props.time` is 60 milliseconds
 * @type {Object}
 */
TW.defaultProps = { time: 60, style: null, name: 'data' };
