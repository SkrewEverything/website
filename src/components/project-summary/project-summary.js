import React from 'react';
import s from './project-summary.css';

/**
 * It is used to display a image, small description and link to the project.
 * It is displayed in `projects` page under `introduction` component.
 * *************************
 * *************************
 * Description of props:
 *
 * "Needed props"
 * name: `this.props.summary`, type:`String or Array of Strings` -> If we have multiple paragraphs to display as a summary of a project, pass it as an `Array of Strings`. Otherwise pass it as a `String`.
 * name: `this.props.image`, type:`Path of Image` -> It is used to display the image of the project above the summary of the project.
 * name: `this.props.link`, type:`Object` -> Link to the project's page.
 * 		 link:{
 * 		 	address: Project site link
 * 		 	target: How to open that link
 * 		 }
 *
 */

export class PS extends React.Component {
	render() {
		/**
		 * Checks whether `this.props.summary` is array or not
		 */

		if (this.props.summary instanceof Array) {
			var summary = [];
			for (var i = 0; i < this.props.summary.length; i++) {
				summary.push(
					<p key={i} className={s['summary']}>
						{this.props.summary[i]}
					</p>
				);
			}
		} else {
			var summary = <p className={s['summary']}>{this.props.summary}</p>;
		}
		return (
			<div className={s['main-div']}>
				<img
					className={s['project-icon']}
					src={this.props.image}
					draggable="false"
				/>
				<h2>{this.props.title}</h2>
				<div>{summary}</div>
				<a
					className={s['project-site']}
					href={this.props.link.address}
					target={this.props.link.target}>
					More Here
				</a>
				<hr />
			</div>
		);
	}
}
