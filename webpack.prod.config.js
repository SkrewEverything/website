const HTMLWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer')
	.BundleAnalyzerPlugin;
const Path = require('path');
var webpack = require('webpack');

module.exports = {
	entry: {
		home: './src/home/index.js',
		projects: './src/projects/index.js',
		vendor: ['react', 'react-dom']
	},

	module: {
		loaders: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				loader: 'babel-loader'
			},
			{
				test: /\.css$/,
				use: ExtractTextPlugin.extract({
					fallback: 'style-loader',
					use:
						'css-loader?modules,localIdentName="[name]-[local]-[hash:base64:6]"!postcss-loader'
				})
			},
			{
				test: /\.(gif|png|jpe?g|svg|webp)$/i,
				loaders: 'file-loader',

				options: {
					name: '/images/[hash].[ext]' //absolute path is required for server
				}
			}
		]
	},

	plugins: [
		new ExtractTextPlugin({
			filename: '[name]/assets/css/index.css'
		}),

		new HTMLWebpackPlugin({
			template: __dirname + '/src/home/index.html',
			filename: __dirname + '/bin/home/index.html',
			chunksSortMode: 'manual',
			chunks: ['manifest', 'vendor', 'home'],
			inject: 'body',
			title: 'Home'
		}),

		new HTMLWebpackPlugin({
			template: __dirname + '/src/projects/index.html',
			filename: __dirname + '/bin/projects/index.html',
			chunksSortMode: 'manual',
			chunks: ['manifest', 'vendor', 'projects'],
			inject: 'body',
			title: 'Projects'
		}),

		new HTMLWebpackPlugin({
			template: __dirname + '/src/home/index.html',
			filename: __dirname + '/bin/index.html',
			chunksSortMode: 'manual',
			chunks: ['manifest', 'vendor', 'home'],
			inject: 'body',
			title: 'Home'
		}),
		new webpack.optimize.CommonsChunkPlugin({
			name: 'vendor',
			filename: 'vendor.bundle.js'
		}),
		new webpack.optimize.CommonsChunkPlugin({
			name: 'manifest',
			filename: 'manifest.bundle.js'
		}),

		new BundleAnalyzerPlugin()
	],

	output: {
		filename: '[name]/index.js',
		path: Path.resolve(__dirname, 'bin')
	}
};
